ics-ans-role-backup-client
===================

Ansible role to install client side requirements of ElkarBackup.  Today this
consists of a yum package and an ssh key in ~/.ssh/authorized_keys.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
backup_client_ssh_user: root
backup_client_ssh_key: "ssh-rsa AAAA[snip]"
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-backup-client
```

License
-------

BSD 2-clause
