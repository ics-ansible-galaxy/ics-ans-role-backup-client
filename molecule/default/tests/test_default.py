import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_rsync_is_installed(host):
    rsync = host.package("rsync")
    assert rsync.is_installed


def test_ssh_key_installed(host):
    ssh_key = host.file("/root/.ssh/authorized_keys")
    assert ssh_key.contains("Automatically generated key for elkarbackup.")
